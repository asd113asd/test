module.exports = {
  "platform": "bitbucket",
  "gitAuthor": "Renovate bot <istoyanov@atlassian.com>",
  "onboarding": true,
  "onboardingConfig": {
    "extends": ["config:base"]
  },
  baseDir: `${process.env.BITBUCKET_CLONE_DIR}/renovate-config`,
  "repositories": ["asd113asd/test"]
}